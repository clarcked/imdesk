import { contextBridge, ipcRenderer } from "electron";

contextBridge.exposeInMainWorld("api", {
  send: (channel, data) => {
    const validChannels = ["db", "bat", "open", "close"];
    if (!validChannels.includes(channel)) return;
    return ipcRenderer.send(channel, data);
  },
  receive: (channel, func) => {
    const validChannels = ["db-entity-created","db-success", "db-error"];
    if (!validChannels.includes(channel)) return;
    return ipcRenderer.on(channel, (_event, args) => func(args));
  },
});
