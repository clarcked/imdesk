import { BrowserWindow } from "electron"

class TaskHandler {
  app;
  parent;
  tasks = {
    splash: {
      url: SPLASH_WINDOW_WEBPACK_ENTRY,
      webPreferences: {
        nodeIntegration: true,
        preload: SPLASH_WINDOW_PRELOAD_WEBPACK_ENTRY,
      },
    },
    home: {
      url: HOME_WINDOW_WEBPACK_ENTRY,
      show: false,
      height: 720,
      width: 1280,
      center: true,
      title: "ImDesk",
      webPreferences: {
        nodeIntegration: true,
        preload: HOME_WINDOW_PRELOAD_WEBPACK_ENTRY,
      },
    },
    stock: {
      url: STOCK_WINDOW_WEBPACK_ENTRY,
      show: false,
      height: 680,
      width: 980,
      center: true,
      alwaysOnTop: true,
      resizable: false,
      title: "Stock",
      webPreferences: {
        nodeIntegration: true,
        preload: STOCK_WINDOW_PRELOAD_WEBPACK_ENTRY,
      },
    },
    entries: {
      url: ENTRIES_WINDOW_WEBPACK_ENTRY,
      show: false,
      height: 680,
      width: 980,
      center: true,
      alwaysOnTop: true,
      resizable: false,
      title: "Entries",
      webPreferences: {
        nodeIntegration: true,
        preload: ENTRIES_WINDOW_PRELOAD_WEBPACK_ENTRY,
      },
    },
    settings: {
      url: SETTINGS_WINDOW_WEBPACK_ENTRY,
      show: false,
      height: 500,
      width: 600,
      center: true,
      title: "Settings",
      resizable: false,
      alwaysOnTop: true,
      webPreferences: {
        nodeIntegration: true,
        preload: SETTINGS_WINDOW_PRELOAD_WEBPACK_ENTRY,
      },
    },
  }

  constructor(app, parent = false) {
    this.app = app
    this.parent = parent
  }

  get_task(NAME) {
    let win;
    let task = Object.keys(this.tasks).includes(NAME) ? this.tasks[NAME] : this.tasks['home']
    try {
      win = new BrowserWindow(task)
      if (this.parent) win.setParentWindow(this.parent)
      win.loadURL(task.url)
      win.once("ready-to-show", () => win.show());
    } catch (error) {
      console.warn(error.message)
    }
    return win
  }
}

export default TaskHandler