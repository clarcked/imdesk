export const open = (name, state = null) => {
  window.api.send("open", { task: name, state });
};

export const close = (name, state = null) => {
  window.api.send("close");
};

export const db = (data, success, error, callBack) => {
  window.api.send(`db`, data);
  window.api.receive(`db-success`, callBack);
  window.api.receive(`db-entity-created`, success);
  window.api.receive(`db-error`, error);
};
