import { DataTypes, Model } from "sequelize"
import DB from './db'

class Stock extends Model { }

Stock.init(
  {
    code: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    variety: {
      type: DataTypes.STRING,
      allowNull: true
    },
    container: {
      type: DataTypes.STRING,
      allowNull: true
    },
    price: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    quantity: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    cost: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    tax: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
  }, {
  sequelize: new DB().get_instance(),
  modelName: 'Stocks',
})


export default Stock