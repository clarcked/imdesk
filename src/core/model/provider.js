import { DataTypes, Model } from 'sequelize'
import DB from './db'


class Provider extends Model { }
Provider.init(
  {
    name: {
      type: DataTypes.STRING
    },
    ref: {
      type: DataTypes.STRING
    }
  },
  {
    sequelize: new DB().get_instance(),
    modelName: 'Providers'
  })

export default Provider