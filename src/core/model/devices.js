import { DataTypes, Model } from "sequelize"
import DB from "./db"


class Device extends Model { }
Device.init(
  {
    name: {
      type: DataTypes.STRING
    },
    provider: {
      type: DataTypes.STRING
    },
    type: {
      type: DataTypes.STRING
    },
    port: {
      type: DataTypes.STRING
    },
    host: {
      type: DataTypes.STRING
    },
  },
  {
    sequelize: new DB().get_instance(),
    modelName: "Devices"
  }
)
export default Device