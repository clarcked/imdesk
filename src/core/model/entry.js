import { DataTypes, Model } from 'sequelize'
import DB from "./db"

class Entry extends Model { }

Entry.init(
  {
    receipt_date: {
      type: DataTypes.DATE
    },
    stocking_date: {
      type: DataTypes.DATE
    },
    provider: {
      type: DataTypes.INTEGER
    },
  },
  {
    sequelize: new DB().get_instance(),
    modelName: 'Entries',
  })


export default Entry