import { DataTypes, Model } from "sequelize"
import DB from "./db"



class Licence extends Model { }
Licence.init(
  {
    device: {
      type: DataTypes.STRING
    },
    key: {
      type: DataTypes.STRING
    },
  },
  {
    sequelize: new DB().get_instance(),
    modelName: "Licences"
  }
)
export default Licence