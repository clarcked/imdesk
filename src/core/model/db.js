import { Sequelize } from "sequelize";

class DB {
  instance;
  constructor(path = "db.sqlite") {
    this.instance = new Sequelize({
      dialect: "sqlite",
      storage: path
    })
  }
  get_instance = () => this.instance
}


export default DB  