import { ipcMain } from "electron"
import Licence from "../core/model/licence";
import Company from "../core/model/company"
import Entry from "../core/model/entry"
import Stock from "../core/model/stock"
import Provider from "../core/model/provider"
import TaskHandler from "../core/task"
let spawn = require("child_process").spawn;


/**
 * Class for control of the application
 * responsible for the creation of the different windows 
 **/
class Controller {

  constructor(app, win) {
    this.app = app
    this.window = win
    this.db_actions = {
      "models": {
        "company": Company,
        "licence": Licence,
        "provider": Provider,
        "entry": Entry,
        "stock": Stock
      },
      "ops": {
        "read": this.read.bind(this),
        "fetch": this.fetch.bind(this),
        "create": this.create.bind(this),
        "update": this.update.bind(this),
        "remove": this.remove.bind(this),
      }
    }

    this.actions = {
      "db": this.db.bind(this),
      "bat": this.bat.bind(this),
      "open": this.open.bind(this),
      "close": this.close.bind(this),
    }

  }


  async read(event, model, arg) {
    try{
       await model.findByPk(arg.data.id)
    }catch(error){
      console.log(error.message)
    }
  }

  async fetch(event, model) {
    try{
       await model.findAll()
    }catch(error){
      console.log(error.message)
    }
  }

  async create(event, model, arg) {
    try{
      const res = await model.create(arg.data) 
      console.log(JSON.stringify(res.dataValues))
      event.reply('db-entity-created', { entity: arg.entity, data: res.dataValues  })
      console.log(`A new ${arg.entity} has been created`,"db-entity-created")
    }catch(error){
      console.log(error.message)
      event.reply("db-error", error);
    }
  }

  async update(event, model, arg) {
    try{
       await model.update(arg.data)
    }catch(error){
      console.log(error.message)
    }
  }

  /**
   * 
   * model.destroy({  where: {  author: 'Brian' }  }) 
   *
   **/
  async remove(event, model, arg) {
    try{
       await model.destroy(arg.data)
    }catch(error){
      console.log(error.message)
    }
  }

  async db(event, args) {
    try {
      if (Array.isArray(args)) {
        args.forEach(async (arg, i) => {
          const Model = this.db_actions['models'][arg.entity]
          await Model.sync()
          await this.db_actions['ops'][arg.operation](event, Model, arg)
          if ((args.length - 1) === i) {
            event.reply('db-success', {})
            console.log("Database transaction terminated", "db-success")
          }
        })
      }
    } catch (error) {
      event.reply("db-error", error);
      console.log(error.message, "db-error")
    }
  }


  /**
   * Action for opening a new window that return a event
   * - opened : the window was opened 
   **/
  async open(event, data = null) {
    try {
      const parent = (data && data['has_parent']) ? this.window: null
      const tsk = (new TaskHandler(this.app, parent)).get_task(data['task'])
      console.log(JSON.stringify(tsk), "new window opened")
      event.reply("opened", data)
    } catch (err) {
      console.log(err.message, "Error while opening window")
    }
  }

  async close() {
    try {
      if (this.window) {
        this.window.close()
      }
    } catch (err) {
      console.log(err.message)
    }
  }

  /**
   * action for executing bat file or teminal command
   **/
  async bat() {
    try {
      let bat = spawn("cmd.exe", ["/c", "test.bat"]);

      bat.stdout.on("data", (data) => {
        console.log(data, "bat stdout")
      });

      bat.stderr.on("data", (err) => {
        console.log(err, "bat stderr")
      });

      bat.on("exit", (code) => {
        console.log(code, "bat exit")
      });
    } catch (error) {
      console.log(error.message)
    }
  }


  /**
   * Here we are seting up the ipc events for each action 
   **/
  exec() {
    Object.keys(this.actions).forEach(action => ipcMain.on(action, this.actions[action]))
  }
}

export default Controller