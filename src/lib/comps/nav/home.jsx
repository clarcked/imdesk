import React from "react"
import { BsFilterLeft } from "react-icons/bs"
import { AiFillSetting } from "react-icons/ai"
import { RiShoppingCart2Fill, RiStore2Fill } from "react-icons/ri"
import { BiSupport } from "react-icons/bi"
import { HiSupport } from "react-icons/hi"
import { Link } from "react-router-dom"
import { open } from "../../../core/action"


function HomeNav() {
  return (
    <nav id="homenav" className="nav">
      <ul>
        <li><Link to="/"><BsFilterLeft /></Link></li>
      </ul>
      <ul>
        <li><Link to="/"><RiShoppingCart2Fill /></Link></li>
        <li><Link to="/stocks"><RiStore2Fill /></Link></li>
        <li><Link to="/help"><HiSupport /></Link></li>
        <li><a onClick={() => open("about")}><BiSupport /></a></li>
      </ul>
      <ul>
        <li><a onClick={() => open("settings")}><AiFillSetting /></a></li>
      </ul>
    </nav>
  )
}


export default HomeNav