import React from "react"
import { MdClose } from "react-icons/md"


function Modal(props) {
  const { title, visible, children, id, onClose } = props

  if (!visible) return null;
  return (
    <div id={id} className="modal">
      <div className="modal-container">
        <div className="modal-header">
          <div className="modal-title">{title}</div>
          <div className="modal-actions">
            <a className="action" onClick={() => onClose(false)}><MdClose /></a>
          </div>
        </div>
        <div className="modal-content">{children}</div>
      </div>
    </div>
  )
}




export default Modal