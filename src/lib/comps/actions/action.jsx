import React from "react";

function Action(props) {
    return (
        <button type="button" className={`action ${props.className}`} {...props}>
            {props.children}
        </button>
    );
}

export default Action;
