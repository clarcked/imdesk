import React from "react";

export function ActionLeft(props) {
    return <div className="action-left">{props.children}</div>;
}

export function ActionRight(props) {
    return <div className="action-right">{props.children}</div>;
}

function Actions(props) {
    return <div className={`actions ${props.className}`}>{props.children}</div>;
}

export default Actions;
