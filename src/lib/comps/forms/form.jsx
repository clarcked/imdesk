import React from "react";

export default function Form(props) {
    return (
        <form action="#" ref={props.form} className="form" {...props}>
            {props.children}
        </form>
    );
}
