import React, { useState } from "react";
import { FaToggleOn, FaToggleOff } from "react-icons/fa";

export default function Toggle(props) {
    const [checked, setStatus] = useState(props.defaultChecked);
    const onClick = () => {
        setStatus(!checked);
        props.onSwitch(!checked);
    };
    return (
        <div id={props.id} className="toggle field">
            <label className="label">{props.label}</label>
            <button type='button' onClick={onClick} className="icon">
                {checked ? <FaToggleOn /> : <FaToggleOff />}
            </button>
        </div>
    );
}
