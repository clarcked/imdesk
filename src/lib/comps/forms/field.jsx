import React from "react";
import { AiOutlineInfoCircle } from "react-icons/ai";
import { GrInfo } from "react-icons/gr";
import { BiError } from "react-icons/bi";

export default function Field(props) {
    const { IconLeft, IconRight } = props
    return (
        <div className="field">
            {props.label && <label className="label">{props.label}</label>}
            <div className="control">
                {IconLeft && <div className="icon icon-left"><IconLeft /></div>}
                {props.children}
                {IconRight && <div className="icon icon-left"><IconRight /></div>}
            </div>

            <div className="notice">
                {props.tip && (
                    <div className="tip">
                        <span className="icon">
                            <AiOutlineInfoCircle />
                        </span>
                        <span>{props.tip}</span>
                    </div>
                )}
                {props.warning && (
                    <div className="warning">
                        <span className="icon">
                            <GrInfo />
                        </span>
                        <span>{props.warning}</span>
                    </div>
                )}
                {props.error && (
                    <div className="error">
                        <span className="icon">
                            <BiError />
                        </span>
                        <span>{props.error}</span>
                    </div>
                )}
            </div>
        </div>
    );
}
