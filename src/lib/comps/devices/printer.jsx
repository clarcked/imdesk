import React from "react";
import { Toggle } from "../forms";
import { t } from "../../../core/locale";
import { AiFillPrinter } from "react-icons/ai";
import Device from "./device.jsx";

export default function Printer() {
    return (
        <Device label={t("Printer")} className="printer" Icon={AiFillPrinter}>
            <div className="body">
                <div className="warning flash">This feature is not available</div>
            </div>
        </Device>
    );
}
