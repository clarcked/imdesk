import React from "react";
import Device from "./device.jsx";
import { FaBalanceScale } from "react-icons/fa";

export default function Balance() {
    return (
        <Device label="Balance" className="balance" Icon={FaBalanceScale}>
            <div className="body">
                <div className="warning flash">This feature is not available</div>
            </div>
        </Device>
    );
}
