import React from "react";
import { t } from "../../../core/locale";
import { ImBarcode } from "react-icons/im";
import Device from "./device.jsx";

export default function Scanner() {
    return (
        <Device label={t("Scanner")} className="scanner" Icon={ImBarcode}>
            <div className="body">
                <div className="warning flash">This feature is not available</div>
            </div>
        </Device>
    );
}
