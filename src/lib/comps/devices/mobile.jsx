import React from "react";
import { t } from "../../../core/locale";
import { FaMobileAlt } from "react-icons/fa";
import Device from "./device.jsx";

export default function Mobile() {
    return (
        <Device label={t("Mobile")} className="mobile" Icon={FaMobileAlt}>
            <div className="body">
                <div className="warning flash">This feature is not available</div>
            </div>
        </Device>
    );
}
