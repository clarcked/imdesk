import React, { useState } from "react";
import { Toggle } from "../forms";

export default function Device(props) {
    const [isopen, open] = useState(props.defaultState);

    const onSwitch = (arg) => {
        open(arg);
    };

    return (
        <div className={`device ${props.className && props.className}`}>
            <div className="header form">
                <div className="icon">{props.Icon && <props.Icon />}</div>
                <Toggle label={props.label} onSwitch={onSwitch} />
            </div>
            {isopen && <React.Fragment>{props.children}</React.Fragment>}
        </div>
    );
}
