import React, { useEffect } from "react";
import { applyToAll, removeClass, setClass } from "../../dom";

function Tabs(props) {

    const getTab = (el) => document.getElementById(el.getAttribute("data-target"));
    const onEvent = (e) => {
        applyToAll(document.getElementsByClassName("tab-title"), (el) => removeClass(getTab(el), "active"));
        setClass(getTab(e.target), "active");
    };

    useEffect(() => {
        const items = document.getElementsByClassName("tab-title");
        applyToAll(items, (el) => el.addEventListener("click", onEvent));
        return () => {
            applyToAll(items, (el) => el.removeEventListener("click", onEvent));
        };
    }, [onEvent]);

    return <div className={`tabs ${props.className}`}>{props.children}</div>;
}

export default Tabs;
