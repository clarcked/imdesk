import React from "react";

function Tab(props) {
    return (
        <a id={props.id} className={`tab ${props.className}`}>
            <div className={`tab-title`} data-target={props.id}>
                {props.title}
            </div>
            <div className={`tab-content`}>{props.children}</div>
        </a>
    );
}

export default Tab;
