export const applyToAll = (items, callBack) => {
    for (let i = 0; i < items.length; i++) callBack(items[i]);
};

export const hasClass = (el, name) => (el ? el.classList.contains(name) : false);

export const setClass = (el, name) => {
    if (el) {
        !hasClass(el, name) && el.classList.add(name);
    }
};

export const removeClass = (el, name) => {
    if (el) {
        hasClass(el, name) && el.classList.remove(name);
    }
};

export const toggleClass = (el, name) => (el ? el.classList.toggle(name) : false);

export const toggleToAllClass = (from, to) => {
    const items = document.getElementsByClassName(from);
    applyToAll(items, (el) => toggleClass(el, to));
};
