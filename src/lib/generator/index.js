
export const genCode39 = (id, val, size = "0.5") => {
    try {
        const divDOM = document.getElementById(id);
        if (divDOM) {
            const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
            svg.setAttribute("jsbarcode-format", "CODE39");
            svg.setAttribute("jsbarcode-value", val);
            svg.setAttribute("jsbarcode-width", "1");
            svg.setAttribute("jsbarcode-height", "50");
            svg.setAttribute("jsbarcode-textmargin", "0");
            svg.setAttribute("jsbarcode-margin", "0");
            svg.setAttribute("jsbarcode-fontoptions", "bold 8px");
            svg.setAttribute("jsbarcode-ratio", "0.5");
            svg.className.baseVal = id;
            divDOM.appendChild(svg);
            divDOM.style.fontSize = "7";
            JsBarcode(`.${id}`).init();
        }
    } catch (error) {
        console.log(error.message);
    }
};

export const genQRCode = (id, val) => {
    const target = document.getElementById(id);
    if (target) {
        const qrcode = new QRCode(target, {
            text: val,
            width: 128,
            height: 128,
            colorDark: "#000000",
            colorLight: "#ffffff",
            correctLevel: QRCode.CorrectLevel.H,
        });
        return qrcode;
    }
};
