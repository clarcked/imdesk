import React from 'react';


function EntryCard(props) {
  const { data } = props

  return (
    <div className="entry-card">
      <div className="thumbnail">
        <div className="icon">
          <span className="txt-up">
            {data?.name.toString().charAt(0)}
            {data?.name.toString().charAt(1)}
          </span>
        </div>
      </div>
      <div className="details">
        <div className="code txt-sm">Code: {data?.code}</div>
        <div className="title txt-b">{data?.name}</div>
        <div className="description">
          <StockDetail data={data} />
        </div>
      </div>
      <div className="stock-actions">
        <a className="action" onClick={props.onEdit}>
          <div className="icon"><MdEdit /></div>
        </a>
        <a className="action" onClick={props.onRemove}>
          <div className="icon"><MdClose /></div>
        </a>
      </div>
    </div>
  );
}



export default EntryCard;