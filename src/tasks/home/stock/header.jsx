import React from 'react'
import { RiEditFill } from 'react-icons/ri'
import { Field } from "../../../lib/comps/forms"
import { open } from "../../../core/action"

function StockHeader(props) {
  const { callback } = props
  return (
    <div className="stock-header">
      <div className="stock-header-content">
        <div className="form">
          <Field>
            <input type="text" name="search" placeholder="Search" />
          </Field>
        </div>
      </div>
      <div className="stock-header-actions">
        <a className="action" onClick={() => open("entries", { has_parent: true })}><RiEditFill /></a>
      </div>
    </div>
  )
}

export default StockHeader