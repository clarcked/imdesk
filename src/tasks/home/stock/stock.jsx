import React, { useEffect } from 'react';
import { open } from "../../../core/action"
import StockHeader from './header.jsx';
import EntryCard from "./card/entry.jsx"


function Stock(props) {
  const { entries } = props;
  const [stock, set_stock] = React.useState([]);

  const on_fetch = (data) => {
    console.log(data, 'on_fetch stock')
  }

  const on_fetch_error = (error) => {
    console.log(error.message)
  }

  const on_create = (data) => {
    console.log(data, "on_create stock")
  }

  const on_create_error = (error) => {
    console.log(error.message, "on_create_error")
  }

  useEffect(() => {
    console.log("loading all entries from the database")
  }, []);
  
  return (
    <div id="stock">
      <StockHeader callback={() => console.log('new stock')} />
      <section>
        <div className="row a-stretch ">
          <div className="col vw-65">
            <div className="entries">
              {entries?.map((val, key) => <EntryCard
                key={key}
                onRemove={() => open("entries", {})}
                onEdit={() => set_stock(entries[key])}
                data={val} />
              )}
            </div>
          </div>
          <div className="col flex-1">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero tempore molestiae, expedita inventore nemo nihil ipsam iste dolorum ad voluptatibus cumque dicta, atque aspernatur dolor aut! Iusto repellat voluptatibus asperiores?
          </div>
        </div>
      </section>
    </div>
  )
}
export default Stock;