import React from "react"
import { Route, Switch } from "react-router-dom"
import { HomeNav } from "../../lib/comps/nav"
import { StatusBottom } from "../../lib/comps/status"
import { Activity } from "./activity"
import { Stock } from "./stock"

function App() {
  return (
    <div id="home">
      <div className="container">
        <HomeNav />
        <section>
          <Switch>
            <Route path="/" component={Activity} exact />
            <Route path="/stocks" component={Stock} />
          </Switch>
        </section>
      </div>
      <StatusBottom />
    </div>
  )
}

export default App