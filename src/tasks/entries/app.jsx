import React, { useState, useEffect } from "react"
import { StatusBottom } from "../../lib/comps/status"
import { Modal } from "../../lib/comps/modal"
import { EntryForm, StockForm } from "./form"
import { EntryCard, StockCard } from "./card"

/***
 * {
    code: "000000000000",
    container: "Botella",
    cost: "9999",
    name: "Cerveza",
    price: "1022.5",
    quantity: "99",
    tax: "0.8",
    variety: "Red IPA"
  }
 */


function App() {
  const [is_entry_form_open, open_entry_form] = useState(false)
  const [entry, set_entry] = useState(null);
  const [edit, set_edit] = useState(null);
  const [stocks, add_stock] = useState([]);

  const on_finish = () => {
    console.log(stocks, 'on_finish stocks')
    console.log(entry, 'on_finish entry')
  }
  const on_fetch = (data) => {
    console.log(data)
  }

  const on_error = (error) => {
    console.log(error.message)
  }

  useEffect(() => {
    if (edit) console.log(edit, 'use effect edit')
    if (stocks) console.log(stocks, 'use effect stock')
    if (entry) console.log(entry, 'use effect entry')
  }, [])

  return (
    <div id="entries">
      <div className="container ">
        <EntryCard {...entry} callback={() => open_entry_form(!is_entry_form_open)} />
        <section>
          <div className="row a-stretch ">
            <div className="col vw-65">
              <div className="stocks">
                {stocks?.map((val, key) => <StockCard
                  key={key}
                  onRemove={() => add_stock(stocks.filter((o, k) => k != key))}
                  onEdit={() => set_edit(stocks[key])}
                  data={val} />)}
              </div>
            </div>
            <div className="col flex-1">
              <StockForm onFinish={on_finish} onSubmit={(data) => { data && add_stock([...stocks, data]) }} />
            </div>
          </div>
        </section>
      </div>
      <Modal id="entry-form" visible={is_entry_form_open} onClose={open_entry_form}>
        <EntryForm {...entry} onSubmit={(data) => { set_entry(data); open_entry_form(false) }} onClose={open_entry_form} />
      </Modal>
      <StatusBottom />
    </div>
  )
}

export default App