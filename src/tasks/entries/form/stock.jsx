import React from "react"
import { Form, Field } from "../../../lib/comps/forms"
import { Actions, Action, ActionLeft, ActionRight } from "../../../lib/comps/actions"
import { t } from "../../../core/locale"
import { useForm } from "react-hook-form";




function StockForm(props) {
  const { onSubmit, onFinish } = props
  const { register, handleSubmit, reset } = useForm()
  const submit = (data) => {
    onSubmit && onSubmit(data)
    reset()
  }

  return (
    <Form className="stock-form form" onSubmit={handleSubmit(submit)} >
      <div className="fields">
        <Field label="Product Name">
          <input {...register("name", { required: true })} defaultValue={props.name} />
        </Field>
        <Field label="Product Code">
          <input {...register("code", { required: true })} defaultValue={props.code} />
        </Field>
        <div className="row gap">
          <div className="col">
            <Field label="Price">
              <input {...register("price", { required: true })} defaultValue={props.price} />
            </Field>
          </div>
          <div className="col">
            <Field label="Quantity" >
              <input  {...register("quantity", { required: true })} defaultValue={props.quantity} />
            </Field>
          </div>
        </div>
        <div className="row gap">
          <div className="col">
            <Field label="Cost">
              <input {...register("cost", { required: true })} defaultValue={props.cost} />
            </Field>
          </div>
          <div className="col">
            <Field label="Tax" >
              <input  {...register("tax", { required: true })} defaultValue={props.tax} />
            </Field>
          </div>
        </div>
        <Field label="Variety" >
          <input {...register("variety", { required: true })} defaultValue={props.variety} />
        </Field>
        <Field label="Container" >
          <input  {...register("container", { required: true })} defaultValue={props.container} />
        </Field>
      </div>
      <div className="actions-container">
        <Actions>
          <ActionRight>
            <Action type="submit">{t("Add Product")}</Action>
            <Action onClick={onFinish}>{t("Finish")}</Action>
          </ActionRight>
        </Actions>
      </div>
    </Form>
  )
}


export default StockForm