import React from "react"
import { Form, Field } from "../../../lib/comps/forms"
import { Actions, Action, ActionLeft, ActionRight } from "../../../lib/comps/actions"
import { t } from "../../../core/locale"
import { RiCalendarEventFill, RiTruckLine } from "react-icons/ri"
import { FaSlackHash } from "react-icons/fa"
import { useForm } from "react-hook-form";




function EntryForm(props) {
  const { receipt_date, stocking_date, provider, callback, onSubmit, onClose } = props
  const { register, handleSubmit } = useForm()
  const submit = (data) => {
    onSubmit(data)
  }

  const close = () => {
    onClose(false)
  }
  return (
    <Form className="entry-form form" onSubmit={handleSubmit(submit)} >
      <div className="row gap">
        <div className="col">
          <Field label="Receipt Date" IconLeft={RiCalendarEventFill}>
            <input {...register("receipt_date", { required: true })} defaultValue={receipt_date} />
          </Field>
        </div>
        <div className="col">
          <Field label="Stocking Date" IconLeft={RiCalendarEventFill}>
            <input  {...register("stocking_date", { required: true })} defaultValue={stocking_date} />
          </Field>
        </div>
      </div>
      <Field label="Provider Name" IconLeft={RiTruckLine}>
        <input {...register("provider.name", { required: true })} defaultValue={provider?.name} />
      </Field>
      <Field label="Provider Ref" IconLeft={FaSlackHash}>
        <input  {...register("provider.ref", { required: true })} defaultValue={provider?.ref} />
      </Field>
      <div className="actions-container">
        <Actions>
          <ActionLeft></ActionLeft>
          <ActionRight>
            <Action onClick={close} >{t("Cancel")}</Action>
            <Action type="submit">{t("Continue")}</Action>
          </ActionRight>
        </Actions>
      </div>
    </Form>
  )
}


export default EntryForm