import React from "react"

function StockDetail(props) {
  const { data } = props
  return (
    <div className="stock-detail">
      <div>{data?.variety}, {data?.container}</div>
      <div className="quantities">
        <div className="price">
          <div className="txt-b"><span className="txt-">{data?.price}</span></div>
          <div className="label txt-sm">Price</div>
        </div>
        <div className="quantity">
          <div className="txt-b"><span className="txt-">{data?.quantity}</span></div>
          <div className="label txt-sm">Quantity</div>
        </div>
        <div className="cost">
          <div className="txt-b"><span className="txt-">{data?.cost}</span></div>
          <div className="label txt-sm">Cost</div>
        </div>
        <div className="tax">
          <div className="txt-b"><span className="txt-">{data?.tax}</span></div>
          <div className="label txt-sm">Taxes</div>
        </div>
      </div>
    </div>
  )
}


export default StockDetail