import React from "react"
import { RiEditFill } from "react-icons/ri"



function EntryCard(props) {
  const { receipt_date, stocking_date, provider, callback } = props
  return (
    <div className="entry-card">
      <div className="entry-dates">
        <div className="row">
          <div className="col">
            <div className="receipt">
              <div className="title">Receipt Date</div>
              <div className="description">{receipt_date ?? "-"}</div>
            </div>
          </div>
          <div className="col">
            <div className="stocking">
              <div className="title">Stocking Date</div>
              <div className="description">{stocking_date ?? "-"}</div>
            </div>
          </div>
        </div>
      </div>
      <div className="entry-provider">
        <div className="name">
          <div className="title">Provider Name</div>
          <div className="description">{provider?.name ?? "-"}</div>
        </div>
        <div className="ref">
          <div className="title"></div>
          <div className="description">{provider?.ref ?? "-"}</div></div>
      </div>
      <div className="entry-card-actions">
        <a className="action" onClick={callback}><RiEditFill /></a>
      </div>
    </div>
  )
}


export default EntryCard