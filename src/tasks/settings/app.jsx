import React, { useEffect } from "react";
import { db, open, close } from "../../core/action";
import {
    Action,
    Actions,
    ActionLeft,
    ActionRight,
} from "../../lib/comps/actions";
import { Tab, Tabs } from "../../lib/comps/tabs";
import "../../lib/generator/barcode";
import "../../lib/generator/qrcode";
import { genCode39, genQRCode } from "../../lib/generator";
import { t, locale } from "../../core/locale";
import General from "./general.jsx";
import Licence from "./licence.jsx";
import Resources from "./resources.jsx";
import { useForm } from "react-hook-form";
import { Form } from "../../lib/comps/forms";

function App() {
    const { register, handleSubmit } = useForm();
    useEffect(() => {
        console.log(locale);
        genCode39("barcode", "12345678", "0.2");
        genQRCode("qrcode", "Hello World");
    }, []);

    const call_back = ()=>{
        console.log("in the call_back")
        open('home')
        close()
    }

    const on_submit = (data) => {
        console.log(data);
    };

    const on_error = (err) => {
        console.log(err.message);
    };

    const submit = (data) => {
        const ops = Object.keys(data).map((item) => ({
            operation: 'create',
            entity: item,
            data: data[item],
        }));
        db(ops, on_submit, on_error, call_back);
    };

    const cancel = () => {
        console.log("canceled");
    };

    return (
        <Form id="settings" onSubmit={handleSubmit(submit)}>
            <div className="tabs-container">
                <Tabs>
                    <Tab id="general" title={t("General")} className="active">
                        <General register={register} />
                    </Tab>
                    <Tab id="resources" title={t("Resources")}>
                        <Resources />
                    </Tab>
                    <Tab id="licences" title={t("Licence Manager")}>
                        <Licence register={register} />
                    </Tab>
                </Tabs>
            </div>
            <div className="actions-container">
                <Actions>
                    <ActionLeft>Lorem ipsum dolor sit amet.</ActionLeft>
                    <ActionRight>
                        <Action onClick={cancel}>{t("Cancel")}</Action>
                        <Action type="submit">{t("Apply Changes")}</Action>
                    </ActionRight>
                </Actions>
            </div>
        </Form>
    );
}

export default App;
