import React from "react";
import { Balance, Scanner, Printer, Mobile } from "../../lib/comps/devices";

export default function Resources() {
    return (
        <div className="resources">
            <Balance />
            <Scanner />
            <Mobile />
            <Printer />
        </div>
    );
}
