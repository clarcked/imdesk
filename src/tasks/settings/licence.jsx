import React from "react";
import { Field } from "../../lib/comps/forms";
import { t } from "../../core/locale";

export default function Licence(props) {
    const { register } = props;
    return (
        <section>
            <Field label={t("Enter Licence Key")}>
                <textarea {...register("licence.key", { required: true })}></textarea>
            </Field>
        </section>
    );
}
