import React from "react";
import { Field } from "../../lib/comps/forms";
import { t } from "../../core/locale";

export default function General(props) {
    const { register } = props;
    return (
        <section id="general-form">
            <Field label={t("Organization")}>
                <input type="text" {...register("company.name", { required: true })} />
            </Field>
            <div className="row gap">
                <div className="col">
                    <Field label={t("Email")}>
                        <input type="text" {...register("company.email", { required: true })} />
                    </Field>
                </div>
                <div className="col">
                    <Field label={t("Phone")}>
                        <input type="text" {...register("company.phone", { required: true })} />
                    </Field>
                </div>
            </div>
            <div className="row gap">
                <div className="col">
                    <Field label={t("Country")}>
                        <input type="text" {...register("company.country", { required: true })} />
                    </Field>
                </div>
                <div className="col">
                    <Field label={t("State")}>
                        <input type="text" {...register("company.state", { required: true })} />
                    </Field>
                </div>
                <div className="col">
                    <Field label={t("ZipCode")}>
                        <input type="text" {...register("company.zipcode", { required: true })} />
                    </Field>
                </div>
            </div>
            <Field label={t("Address")}>
                <textarea {...register("company.address", { required: true })}></textarea>
            </Field>
        </section>
    );
}
